# ml4proflow-data-dummy
This package provides some example data for ml4proflow.

## Installation
Activate your virtual environment (optionally) and
install the package with pip:
```bash 
pip install ml4proflow-data-dummy
```

The example data is located inside your environment at ```$ENV/ml4proflow/data``` 

## Contribution
For development, install this repository in editable mode with pip:
```bash 
git clone https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-data-dummy.git
cd ml4proflow-data-dummy
pip install -e .
```
