from setuptools import setup, find_packages

long_description = ""

name = "ml4proflow-data-dummy"
version = "0.0.1"

cmdclass = {}

try:
    from sphinx.setup_command import BuildDoc
    cmdclass['build_sphinx'] = BuildDoc
except ImportError:
    print('WARNING: Sphinx not available, not building docs')

setup(
    name=name,
    version=version,
    author="Christian Klarhorst",
    author_email="cklarhor@techfak.uni-bielefeld.de",
    description="Dummy files",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-data-dummy",
    project_urls={
        "Main framework": "https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow",
    },
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],

    packages=find_packages(where="src"),
    package_dir={"": "src"},
    package_data={"ml4proflow": ["py.typed"]},
    entry_points={
    },
    cmdclass=cmdclass,
    data_files=[('ml4proflow/data', ['data/citec/5K.csv'])],
    python_requires=">=3.6",
    install_requires=["ml4proflow"],
    extras_require={
        "tests": ["pytest", 
                  "pandas-stubs",
                  "pytest-html",
                  "pytest-cov",
                  "flake8",
                  "mypy"],
        "docs": ["sphinx", "sphinx-rtd-theme", "recommonmark"],
    },
    command_options={
        'build_sphinx': {
            'project': ('setup.py', name),
            'version': ('setup.py', version),
            'release': ('setup.py', version),
            'source_dir': ('setup.py', 'docs/source/'),
            'build_dir': ('setup.py', 'docs/build/')
        }
    },
)
